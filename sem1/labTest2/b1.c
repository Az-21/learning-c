/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART B: Problem 1
    * Write a C program to search for an element in an array using binary search.
*/

//Preprocessor
#include<stdio.h>

//Main
int main(){
    //Variables
    int i, lower, upper, mid, arrLen, numFind;
    int A[50];

    //User input: length of array
    printf("Enter the length of the array: ");
    scanf("%d", &arrLen);

    //User input: populating array
    printf("Populate your array in ascending order\n");
    for(i=0; i<arrLen; i++){
        scanf("%d", &A[i]);
    }

    //User input: number to find
    printf("Enter the number you want to search: ");
    scanf("%d", &numFind);

    lower = 0;
    upper = arrLen;
    //Binary search
    while(lower<=upper){
        mid = (lower+upper)/2;

        if(A[mid]==numFind){
            printf("The number %d is present in the array at index %d\n", numFind, mid);
            return 0;       //terminate the program [FLAG]
        }
        else if(A[mid] < numFind){
            lower = mid + 1;
        }
        else{
            upper = mid -1;
        }
    }

    //If [FLAG] is not triggered, the element is not present in the array
    printf("The number %d is not present in the array\n", numFind);
    return 0;
}