/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART B: Problem 3
    * Write a C program to find whether a given square matrix is symmetric or not.
*/

//Preprocessor
#include<stdio.h>

//Main
int main(){
    //Variables
    int i, j, row;
    int A[10][10];

    //User input: dimension of matrix
    printf("Enter a dimension of the square matrix: ");
    scanf("%d", &row);

    //User input: populating matrix
    printf("Populate the matrix row-wise\n");
    for(i=0; i<row; i++){
        for(j=0; j<row; j++){       //row = col; so only one var was used
            scanf("%d", &A[i][j]);
        }
    }

    //Printing the matrix
    for(i=0; i<row; i++){
        for(j=0; j<row; j++){
            printf("%4d", A[i][j]);
        }
        printf("\n");
    }

    //Checking for symmetry
    for(i=0; i<row; i++){
        for(j=0; j<row; j++){
            if(A[i][j] != A[j][i]){     //[FLAG]
                printf("The matrix is not symmetric\n");
                return 0;       //terminate the program
            }
        }
    }

    //If matrix is symmetric, this codeblock is executed [FLAG] is not triggered
    printf("The matrix is symmetric\n");
    return 0;
}