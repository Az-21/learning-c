/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART A: Problem 1
    * Write a C program to search for an element in an array using linear search.

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    int numArray[50], arrLen, numFind, i, count=0;

    //User input: length of array
    printf("Enter the length of the array: ");
    scanf("%d", &arrLen);

    //User input: populating array
    printf("Populate your array\n");
    for(i=0; i<arrLen; i++){
        scanf("%d", &numArray[i]);
    }    

    //User input: which number to find
    printf("Enter the number you want to find: ");
    scanf("%d", &numFind);

    //Linear search
    for(i=0; i<arrLen; i++){
        if(numArray[i]==numFind){
            printf("The number %d is present in array at index %d\n", numFind, i);
            count++;
        }
    }

    //This section is executed if the number is not present in the array
    if(count==0){
        printf("The number %d is not present in the array\n", numFind);
    }
}