/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART B: Problem 7
    * Write a C program to find the Trace and Norm of the matrix

*/

//Preprocessor
#include<stdio.h>
#include<math.h>

//Main
void main(){
    //Variables
    int i, j, row;
    float A[10][10], trace=0, norm=0;

    /*Definition of trace and norm
        * Valid for only square matrix
        * Trace = sigma(primary diagonal)
        * Norm = sqrt[sigma(Aij^2)]
    */

    //User input: dimension of matrix
    printf("Enter one dimension of a square matrix: ");
    scanf("%d", &row);

    //User input: populating matrix
    printf("Populate the matrix\n");
    for(i=0; i<row; i++){
        for(j=0; j<row; j++){       //row = col
            scanf("%f", &A[i][j]);
        }
    }
    printf("\n");

    //Printing matrix, calculating norm and trace
    for(i=0; i<row; i++){
        for(j=0; j<row; j++){       //row = col
            printf("%4.2f ", A[i][j]);

            //Trace
            if(i==j){
                trace += A[i][j];
            }

            //Norm
            norm += A[i][j] * A[i][j];
        }
        printf("\n");
    }

            
    
    printf("\n");
    norm = sqrt(norm);
    //Output
    printf("Trace = %.3f\n", trace);
    printf("Norm = %.3f\n", norm);

}