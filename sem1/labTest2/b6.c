/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART B: Problem 6
    * Write a C program to find whether a number is a palindrome using functions without arguments, with return type.

*/

//Preprocessor
#include<stdio.h>

//Prototype
int isPal();

//Main
void main(){
    //Variable
    int boolPal = isPal();

    //Palindrome test
    if (boolPal==1){
        printf("Given number is a palindrome\n");
    }
    else{
        printf("Given number is not a palindrome\n");
    }
}



//User defined program
int isPal(){
    //Variables
    int num, numIni, rev=0;

    //User input: number
    printf("Enter a number: ");
    scanf("%d", &num);
    numIni = num;

    while(num>0){
        rev = rev*10 + num%10;
        num = num/10;
    }


    if(rev == numIni){
        return 1;       //true
    }
    else{
        return 0;       //false
    }


}