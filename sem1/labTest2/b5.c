/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART B: Problem 5
    * Write a C program to find GCD and LCM of 2 numbers using functions with arguments, without return type.
*/

//Preprocessor
#include<stdio.h>

//Prototype
void lcmGCD(int x, int y);

//Main
void main(){
    //Variables
    int num1, num2;

    //User input: numbers
    printf("Enter two numbers: ");
    scanf("%d %d", &num1, &num2);

    //LCM and GCD
    lcmGCD(num1, num2);
}


//User defined function
void lcmGCD(int x, int y){
    //Variables
    int greater, i=1, gcd, lcm;
 
    //Greater number
    if(x>y){
        greater = x;
    }
    else{
        greater = y;
    }


    //GCD
    while(i<=greater){
        if(x%i==0 && y%i==0){
            gcd = i;
        }
        i++;
    }

    //LCM
    lcm = (x*y)/gcd;

    //Output
    printf("LCM of %d and %d is %d\n", x, y, lcm);
    printf("GCD of %d and %d is %d\n", x, y, gcd);
}