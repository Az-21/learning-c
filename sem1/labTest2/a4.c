/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART A: Problem 4
    * Write a C program to compare two strings without using built-in functions.

*/

//Preprocessor
#include<stdio.h>

//Main
int main(){
    //Variables
    char str1[20], str2[20];
    int strLen1=0, strLen2=0, i;

    //User input: strings
    printf("Enter string 1: ");
    fgets(str1, 20, stdin);

    printf("Enter string 2: ");
    fgets(str2, 20, stdin);

    //Calculating string lengths
    i=0;
    while(str1[i] != '\0'){
        strLen1++;
        i++;
    }
    i=0;        //re-init
    while(str2[i] != '\0'){
        strLen2++;
        i++;
    }

    //Fail condition 1: lengths are not equal
    if(strLen1 != strLen2){
        printf("\nStrings are not equal: different lengths\n");
        return 0;       //terminate the program
    }

    //Fail condition 2: characters are not equal
    for(i=0; i<=strLen1; i++){
        if(str1[i] != str2[i]){
            printf("\nThe strings are not equal, they differ at index %d\n", i);
            return 0;       //terminate the program
        }
    }

    //Passed. If program reaches this codeblock, the strings are equal
    printf("\nThe strings are equal\n");
    return 0;
}