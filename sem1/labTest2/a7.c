/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART A: Problem 7
    * Write a C program to read a list of positive values and calculate its average using break and continue.

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variable
    int myArray[40], i=0, arrLen, sigma=0, acceptedNumbers;
    float avg;

    //User input
    printf("How many numbers do you want to enter: ");
    scanf("%d", &arrLen);
    
    printf("Enter the numbers one at a time\n");
    while(1){
        scanf("%d", &myArray[i]);
        
        if(myArray[i]>0){                           //0 is neither +ve or -ve; it is the reference
            acceptedNumbers++;
            sigma = sigma+myArray[i];
        }
        
        i++;

        if(i<arrLen){
            continue;
        }
        else{
            break;
        }
    }

    //Output
    avg = (float)sigma/acceptedNumbers;             //of all the numbers entered, only "acceptedNumbers" were +ve
    printf("Average of all positive numbers entered is %.2f\n", avg);


}