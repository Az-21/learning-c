/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART A: Problem 5
    * Write a C program to find factorial of a given number using functions with arguments, with return type.
*/

//Preprocessor
#include<stdio.h>

//Prototype
long forFactorial(int num);

//Main
void main(){
    //Variable
    int num;
    long fact;

    //User input: number
    printf("Enter a number: ");
    scanf("%d", &num);

    //Finding factorial
    fact = forFactorial(num);
    printf("%d! = %ld\n", num, fact);
}



//Factorial function
long forFactorial(int num){
    int i;
    long fact;
    
    if(num<1){
        printf("Error: enter a positive integer\n");
        return -1;
    }
    else{
        fact = 1;
        for(i=num; i>=1; i--){
            fact *= i;
        }
        return fact;
    }
}