/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART B: Problem 4
    * Write a C program to find whether given string is palindrome or not.

*/

//Preprocessor
#include<stdio.h>

//Main
int main(){
    //Variable
    char str[20];
    int i, left, right;

    //User input: string
    printf("Enter a string: ");
    fgets(str, 20, stdin);

    //Length of string
    i=0;           
    while(str[i] != '\0'){
        i++;        //stores length of string inc NULL
    }

    left = 0;
    right = i-2;    /* The above code counts
                     * 1st elements at i=1 (but we want it at 0) => -1
                     * Counts null => -1
                     * Therefore, subtracting 2 to arrive at the correct rightmost non-NULL index
                    */
    //Palindrome test
    while(left<=right){
        if(str[left] != str[right]){    //[FLAG]
            printf("The given string is not a palindrome\n");
            return 0;       //terminate the program
        }
        left++;
        right--;
        
    }

    //If the [FLAG] isn't triggered, string is palindrome
    printf("The given string is a palindrome\n");
    return 0;
}