/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART A: Problem 2
    * Write a C program to search for an element in an array using linear search.

*/

//Preprocessor
#include<stdio.h>

//Main
int main(){
    //Variables
    int i, j, rowA, rowB, colA, colB;
    int A[10][10], B[10][10], C[10][10];

    //User input: dimension of matrix
    printf("[Matrix A] Enter the number of rows and columns: ");
    scanf("%d %d", &rowA, &colA);

    printf("[Matrix B] Enter the number of rows and columns: ");
    scanf("%d %d", &rowB, &colB);

    //Fail condition: dimension of matrix is not equal
    if(rowA!=rowB || colA!=colB){
        printf("Cannot add given matrices because they have different dimension\n");
        return 0;       //terminate the program
    }

    //User input: populating matrices
    printf("[Matrix A] Populate the matrix row-wise\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colA; j++){
            scanf("%d", &A[i][j]);
        }
    }

    printf("[Matrix B] Populate the matrix row-wise\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colA; j++){
            scanf("%d", &B[i][j]);
            C[i][j] = A[i][j] + B[i][j];    //C = A + B
        }
    }

    //Output
    printf("\n\n");
    printf("[Matrix A]\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colA; j++){
            printf("%4d", A[i][j]);
        }
        printf("\n");
    }
    printf("\n\t+\n\n");
    printf("[Matrix B]\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colA; j++){
            printf("%4d", B[i][j]);
        }
        printf("\n");
    }
    printf("\n\t=\n\n");
    printf("[Matrix C]\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colA; j++){
            printf("%4d", C[i][j]);
        }
        printf("\n");
    }



}