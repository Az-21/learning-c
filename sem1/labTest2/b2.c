/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART B: Problem 2
    * Write a C program to sort elements in ascending order using bubble sort.

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    int i, j, arrLen, temp;
    int A[50];

    //User input: length of array
    printf("Enter the length of the array: ");
    scanf("%d", &arrLen);

    //User input: populating array
    printf("Populate your array\n");
    for(i=0; i<arrLen; i++){
        scanf("%d", &A[i]);
    }

    //Bubble sort
    for(i=0; i<arrLen-1; i++){
        for(j=0; j<arrLen-1; j++){
            if(A[j]>A[j+1]){
                temp=A[j+1];
                A[j+1] = A[j];
                A[j] = temp;
            }
        }
    }

    //Output
    for(i=0; i<arrLen; i++){
        printf("\nAfter bubble sort\n%d ", A[i]);
    }
    printf("\n");
}