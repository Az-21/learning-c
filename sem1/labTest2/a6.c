/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART A: Problem 6
    * Write a C program to combine two strings without using built-in functions.

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    char str1[60], str2[30];
    int strLen1, i, j;

    //User input: strings
    printf("Enter string 1: ");
    fgets(str1, 30, stdin);

    printf("Enter string 2: ");
    fgets(str2, 30, stdin);

    //Length of string 1
    i=0;
    while(str1[i] != '\0'){
        i++;    //i stores length of string1
    }

    //Combining the strings
    j=0;
    while(str2[j] != '\0'){
        str1[i-1]=str2[j];  //starting from NULL of string1, string2 is overwritten until the NULL of string2 is reached
        i++;
        j++;
    }

    printf("\nAfter combination\n");
    printf("%s\n", str1);

}