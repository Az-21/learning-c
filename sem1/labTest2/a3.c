/*
    * Made by Az-21
    * Sem 1 Lab Test 2
    * PART A: Problem 3
    * Write a C program to find transpose of a given matrix.

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    int i, j, rowA, colA;
    int A[10][10], B[10][10];

    //User input: dimension of matrix
    printf("Enter the number of rows and columns: ");
    scanf("%d %d", &rowA, &colA);

    //User input: populating matrices
    printf("Populate the matrix row-wise\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colA; j++){
            scanf("%d", &A[i][j]);
            B[j][i] = A[i][j];      //B stores transpose
        }
    }

    //Output
    printf("Transpose of\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colA; j++){
            printf("%4d", A[i][j]);
        }
        printf("\n");
    }

    printf("\n\tis\n\n");
    for(i=0; i<colA; i++){      //RC is switched for non-square matrix
        for(j=0; j<rowA; j++){
            printf("%4d", B[i][j]);
        }
        printf("\n");
    }
}