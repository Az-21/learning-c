/*
    * Made by Az-21
    * Sem 1 Blue Book Assignment
    * Problem 2
    * Write a C program to find the prime numbers within given range using function with argument and without return type.

*/

//Preprocessor
#include<stdio.h>

//Prototype
void isPrime(int num);

//Main
void main(){
    //Variables
    int lower, upper, i;

    //User input: range of numbers
    printf("Enter the lower and upper range: ");
    scanf("%d %d", &lower, &upper);

    //Printing primes
    for(i=lower; i<=upper; i++){
        isPrime(i);
    }

}

void isPrime(int num){
    //Non-argument variable
    int i;

    //Prime check
    if(num<=0){
        printf("Invalid range. Please enter a positive integer\n");
    }
    else if(num==1){
        //If this code block is executed, number is not a prime
    }
    else if(num==2){
        printf("2\n");
    }
    else if(num%2==0){
        //If this code block is executed, number is not a prime
    }
    else{
        for(i=3; i<=num-2; i=i+2){
            if(num%i == 0){
                goto exitLoop;  //I have to use this dumb hack because I'm not allowed to use return type for dumb some reason
            }
        }
        printf("%d\n", i);  //if the goto is not triggered, the number is prime
    
    }
    exitLoop:
    printf("");

}