/*
    * Made by Az-21
    * Sem 1 Blue Book Assignment
    * Problem 3
    * Write a C program to find the row sum, column sum and sum of all elements in an array.

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    int matrix[20][20], row, col, sigmaRow, sigmaCol, sigmaTotal=0, i, j;

    //User input: row and column
    printf("Enter the dimension of the matrix (row and column): ");
    scanf("%d %d", &row, &col);
    printf("\n");

    //User input: populating matrix
    printf("Populate the martix one element at a time. Column is populated first\n");
    for(i=0; i<row; i++){
        for(j=0; j<col; j++){
            scanf("%d", &matrix[i][j]);
        }
    }

    printf("\n");       //formating
    
    //Printing the matrix
    printf("The matrix registered:\n");
    for(i=0; i<row; i++){
        for(j=0; j<col; j++){
            printf("%d  ", matrix[i][j]);
        }
        printf("\n");
    }

    printf("\n");       //formating

    //Column sum
    for(i=0; i<col; i++){
        sigmaRow=0;
        for(j=0; j<row; j++){
            sigmaRow += matrix[j][i];
        }
        printf("Sum of elements of column %d is %d\n", i+1, sigmaRow);
        sigmaTotal += sigmaRow;
    }

    printf("\n");     //formating

    //Row sum
    for(i=0; i<row; i++){
        sigmaCol=0;
        for(j=0; j<col; j++){
            sigmaCol += matrix[i][j];
        }
        printf("Sum of elements of row %d is %d\n", i+1, sigmaCol);
    }

    //Total sum
    printf("\nSum of all elements is %d", sigmaTotal);
}