/*
    * Made by Az-21
    * Sem 1 Blue Book Assignment
    * Problem 1
    * Write a C program to find mean, variance, and standard deviation using an array.

*/

//Preprocessor
#include<stdio.h>
#include<math.h>

//Main
void main(){
    //Variables
    float num[40], mean, var=0, sd, sigma=0;
    int arrLen, i;
    //User input: length of array
    printf("Enter the sample size/length of array: ");
    scanf("%d", &arrLen);

    //User input: array
    printf("\nEnter the elements of the array\n");
    for(i=0; i<arrLen; i++){
        scanf("%f", &num[i]);
        sigma += num[i];
    }

    //Mean = sum of all numbers/elements in array
    mean = sigma/arrLen;

    //Variance = SIGMA((element-mean)^2)/elements-1
    for(i=0; i<arrLen; i++){
        var += (num[i]-mean)*(num[i]-mean);
    }
    var = var/(arrLen-1);

    //Standard deviation = square root of variance
    sd = sqrt(var);

    //Output
    printf("\nMean = %.4f\nVariance = %.4f\nStandard Deviation = %.4f\n", mean, var, sd);
}