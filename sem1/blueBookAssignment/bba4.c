/*
    * Made by Az-21
    * Sem 1 Blue Book Assignment
    * Problem 4
    * Write a C program to find matrix multiplication

*/

//Preprocessor
#include<stdio.h>

//Main
int main(){
    //Variables
    int rowA, colA, rowB, colB, i, j, k;
    int A[20][20], B[20][20], C[20][20];

    //User input: row and column for matrix A
    printf("[Matrix A] Enter the dimension of the matrix (row and column): ");
    scanf("%d %d", &rowA, &colA);

    //User input: row and column for matrix B
    printf("[Matrix B] Enter the dimension of the matrix (row and column): ");
    scanf("%d %d", &rowB, &colB);
    printf("----------\n");

    //User input: populating matrix A
    printf("[Matrix A] Populate the martix one element at a time. Column is populated first\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colA; j++){
            scanf("%d", &A[i][j]);
        }
    }
    printf("\n");       //formating
    
    //User input: populating matrix B
    printf("[Matrix B]Populate the martix one element at a time. Column is populated first\n");
    for(i=0; i<rowB; i++){
        for(j=0; j<colB; j++){
            scanf("%d", &B[i][j]);
        }
    }
    printf("----------\n");       //formating

    //Printing the matrix multiplication
    printf("[Matrix A]:\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colA; j++){
            printf("%4d  ", A[i][j]);
        }
        printf("\n");
    }

    printf("\nx\n\n");

    printf("[Matrix B]:\n");
    for(i=0; i<rowB; i++){
        for(j=0; j<colB; j++){
            printf("%4d  ", B[i][j]);
        }
        printf("\n");
    }
    printf("----------\n");

    

    //Checking for colA=rowB
    if(colA!=rowB){
        printf("Multiplication of the given matrices is not possible\n");
        return 0;       //terminate program
    }


    /*  *Matrix C: AxB
        *Size of product matrix is rowA x colB
        *Step 1: init the elements to zero
        *Step 2: multiply add
    */

    for(i=0; i<rowA; i++){
        for(j=0; j<colB; j++){
            C[i][j] = 0;                        //Step 1
            for(k=0; k<colA; k++){
                C[i][j] += A[i][k] * B[k][j];   //Step 2
            }
        }
    }

    //Output
    printf("[Matrix C] = [Matrix A] x [Matrix B]:\n");
    for(i=0; i<rowA; i++){
        for(j=0; j<colB; j++){
            printf("%4d  ", C[i][j]);
        }
        printf("\n");
    }
    printf("----------\n");


}
