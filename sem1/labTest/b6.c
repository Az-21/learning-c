/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART B: Problem 6

*/

//Preprocessor
#include<stdio.h>
#include<math.h>

//Main
void main(){
    //Variables
    float a, b, c, disc, r1, r2;

    //User input
    printf("Enter the coefficients of standard ax^2+bx+c=0 equation\n");
    scanf("%f %f %f", &a, &b, &c);

    //Calc: discriminant
    disc = b*b-4*a*c;

    //Calc: roots + output
    if(disc>=0){
        disc = sqrt(disc);
        r1 = (-1*b-disc)/(2*a);
        r2 = (-1*b+disc)/(2*a);

        printf("Root 1 = %.2f\n", r1);
        printf("Root 2 = %.2f\n", r2);
    }
    else{                       //for img roots
        disc = -disc;
        disc = sqrt(disc);
        r1 = -1*b/(2*a);        //real part
        r2 = disc/(2*a);        //img part

        printf("Root 1 = %.2f + %.2fi\n", r1, r2);
        printf("Root 2 = %.2f - %.2fi\n", r1, r2);
    }    
}