/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 6

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variable
    int num1, num2, num3;

    //User input
    printf("Enter three numbers: ");
    scanf("%d %d %d", &num1, &num2, &num3);

    //Nested conditional operator: largest num
    (num1>num2)?
        ((num1>num3)?
            (printf("%d", num1))
            :
            (num2>num3)?
                (printf("%d", num2))
                :
                (printf("%d", num3))
        )
        :
        ((num2>num3)?
            (printf("%d", num2))
            :
            (num3>num1)?
                (printf("%d", num3))
                :
                (printf("%d", num1))
        );

    printf(" is the largest number\n");        
}