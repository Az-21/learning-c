/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 8

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variable
    int num, numIni, i;
    long fact;

    //User input
    printf("Enter a number: ");
    scanf("%d", &num);
    numIni = num;
    fact = num;

    //Calculation
    for(i=0; num!=1; i++){
        fact = fact*(num-1);
        num--;
    }

    printf("%d! = %ld\n", numIni, fact);
}