/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 10

*/

//Preprocessor
#include<stdio.h>
#include<ctype.h>

//Main
void main(){
    //Variable
    char ch;

    //User input
    printf("Enter a character: ");
    scanf("%c", &ch);

    //if tests
    if(isalpha(ch)!=0){
        printf("Entered character is an alphabet\n");
    }

    if(isdigit(ch)!=0){
        printf("Entered character is a number\n");
    }

    if(ispunct(ch)!=0){
        printf("Entered character is a punctuation\n");
    }

    if(isspace(ch)!=0){
        printf("Entered character is a whitespace\n");
    }
}