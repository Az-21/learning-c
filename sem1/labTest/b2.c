/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART B: Problem 2

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    int num, i, sigmaOdd=0, sigmaEven=0;

    //User input
    printf("Enter a number: ");
    scanf("%d", &num);

    for(i=0; i<=num; i++){
        if(i%2==0){
            sigmaEven = sigmaEven + i;
        }
        else{
            sigmaOdd = sigmaOdd + i;
        }
    }

    //Output
    printf("Summation of first %d even natural numbers is %d\n", num, sigmaEven);
    printf("Summation of first %d odd natural numbers is %d\n", num, sigmaOdd);

    
}