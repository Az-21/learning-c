/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART B: Problem 4

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variable
    int num, numIni, rev;

    //User input
    printf("Enter a number: ");
    scanf("%d", &num);
    numIni = num;
    rev = 0;

    //Calculation
    while(num!=0){
        rev   = rev*10;
        rev   = rev+(num%10);
        num   = num/10;
    }

    //Check for palindrome + Output
    if(rev==numIni){
        printf("%d is a palindrome", numIni);
    }
    else{
        printf("%d is not a palindrome", numIni);
    }
}