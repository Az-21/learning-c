/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART B: Problem 3
    * GCD*LCM = num

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variable
    int num1, num2, hcf, lcm, i=1, greaterNum;

    //User input
    printf("Enter two numbers: ");
    scanf("%d %d", &num1, &num2);

    //Finding greater number
    if(num1-num2>=0){
        greaterNum = num1;
    }
    else{
        greaterNum = num2;
    }

    //Finding HCF
    do{
        if(num1%i==0 && num2%i==0){
            hcf = i;
        }
        i++;
    }
    while(i<=greaterNum);

    //Finding LCM
    lcm = num1*num2/hcf;

    //Output
    printf("GCD of %d and %d is %d\n", num1, num2, hcf);
    printf("LCM of %d and %d is %d\n", num1, num2, lcm);
}