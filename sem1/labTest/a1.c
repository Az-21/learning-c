/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 1

*/

//Preprocessor
#include<stdio.h>
#include<math.h>

//Main
void main(){
    //Variables
    float x1, x2, y1, y2, dist;

    //User input
    printf("Enter x1 and y1: ");
    scanf("%f %f", &x1, &y1);

    printf("Enter x2 and y2: ");
    scanf("%f %f", &x2, &y2);

    dist = sqrt(((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)));
    printf("The distance between A(%.2f, %.2f) and B(%.2f, %.2f) is %.2f\n", x1, y1, x2, y2, dist);

}