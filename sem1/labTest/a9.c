/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 9

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variable
    int num, numIni, rev, sigma;

    //User input
    printf("Enter a four digit number: ");
    scanf("%d", &num);
    numIni = num;
    rev = 0;
    sigma = 0;

    //Calculation
    while(num!=0){
        rev   = rev*10;
        rev   = rev+(num%10);
        sigma = sigma + num%10;
        num   = num/10;
    }

    //Output
    printf("Reverse of %d is %d\n", numIni, rev);
    printf("Sum of digits is %d\n", sigma);
}