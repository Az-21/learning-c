/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART B: Problem 8

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    float num1, num2, ans;
    char operator;

    //User input
    printf("Enter two numbers followed by an arith operator\n");
    scanf("%f %f %c", &num1, &num2, &operator);

    //Switch statement
    switch(operator){
        case '+':       ans = num1 + num2; break;
        case '-':       ans = num1 - num2; break;
        case '*':       ans = num1 * num2; break;
        case '/':       ans = num1 / num2; break;
        default:        printf("Invalid operator. Valid operators: + - * /\n");
    }

    //Output
    printf("%.2f %c %.2f = %.2f\n", num1, operator, num2, ans);
}