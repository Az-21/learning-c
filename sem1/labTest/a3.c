/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 3

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    float degC, degF;

    //C to F
    printf("Enter the temperature in degree Fahrenheit: ");
    scanf("%f", &degF);

    degC = (degF-32)*(5/9);
    printf("%.2f degree Fahrenheit = %.2f degree Celsius\n\n", degF, degC);

    //F to C
    printf("Enter the temperature in degree Celsius: ");
    scanf("%d", &degC);

    degF = (degC*9/5)+32;
    printf("%.2f degree Celsius = %.2f degree Fahrenheit\n", degC, degF);
}