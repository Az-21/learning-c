/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 2

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    int num1, num2, temp;

    //User input
    printf("Enter two numbers: ");
    scanf("%d %d", &num1, &num2);

    //Swapping memory
    temp = num1;
    num1 = num2;
    num2 = temp;

    //Output
    printf("Num1= %d\nNum2= %d\n", num1, num2);
}