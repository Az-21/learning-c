/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 5

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variable
    char c;

    //User input
    printf("Enter a character: ");
    scanf("%c", &c);

    //Vowel test
    (c=='a'||c=='e'||c=='i'||c=='o'||c=='u'||c=='A'||c=='E'||c=='I'||c=='O'||c=='U')?
        (printf("Entered character is vowel\n"))
        :
        (printf("Entered character is consonant\n"));
}