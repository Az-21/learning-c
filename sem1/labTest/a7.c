/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 7

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variable
    char ch;

    //User input
    printf("Enter a character: ");
    scanf("%c", &ch);

    //Vowel test
    if(ch=='a' || ch=='e' || ch=='i' || ch=='o' || ch=='u' || ch=='A' || ch=='E' || ch=='I' || ch=='O' || ch=='U'){
        printf("Entered character is vowel\n");
    }
    else{
        printf("Entered character is consonant\n");
    }
}