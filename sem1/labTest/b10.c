/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART B: Problem 10

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    int marks[20], noOfSub, i, gpa;
    float avg = 0;

    //User input: length of array
    printf("Enter the number of subjects: ");
    scanf("%d", &noOfSub);

    //User input: input marks and average
    printf("Enter your marks one at a time\n");
    for(i=0; i<noOfSub; i++){
        scanf("%d", &marks[i]);
        avg = avg + marks[i];
    }

    //Average
    avg = avg/noOfSub;

    //Switch statement [OUTPUT]
    printf("Your average is %.2f\nYour grade is ", avg);
    gpa = avg/10;       //stores int on scale of 0-10
    switch(gpa){
        case 10:   printf("S"); break;
        case 9:    printf("S"); break;
        case 8:    printf("A"); break;
        case 7:    printf("B"); break;
        case 6:    printf("C"); break;
        case 5:    printf("D"); break;
        case 4:    printf("E"); break;
        default:   printf("F"); break;

    }
    
    printf("\n");       //for formatting

    /*I'm not sure if the question requires a special condition of F should any subject have less than 40, but if it says so:
        * change the void main() to int main()
        * in the for loop, add an if condition for marks[i]<40
        * print F and return 0;
    */
}