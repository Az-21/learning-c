/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART B: Problem 1

*/

//Preprocessor
#include<stdio.h>
#include<math.h>

//Main
void main(){
    //Variables
    int count=1;
    double num;

    redo_root:                  //used for goto
    //User input
    printf("This is interation number %d\n", count);
    printf("Enter a number: ");
    scanf("%lf", &num); 

    //Calc and output
    printf("Square root of %.2lf is %.2lf\n\n", num, sqrt(num));

    //goto [5 iterations]
    count++;
    if(count<6){
        goto redo_root;
    }
}