/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART B: Problem 7

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    int marks[20], noOfSub, i;
    float avg = 0;

    //User input: length of array
    printf("Enter the number of subjects: ");
    scanf("%d", &noOfSub);

    //User input: input marks and average
    printf("Enter your marks one at a time\n");
    for(i=0; i<noOfSub; i++){
        scanf("%d", &marks[i]);
        avg = avg + marks[i];
    }

    //Average
    avg = avg/noOfSub;

    //If-elseif ladder [OUTPUT]
    printf("Your average is %.2f\nYour grade is ", avg);
    if(avg>=90){
        printf("S");
    }
    else if(avg>=80){
        printf("A");
    }
    else if(avg>=70){
        printf("B");
    }
    else if(avg>=60){
        printf("C");
    }
    else if(avg>=50){
        printf("D");
    }
    else if(avg>=40){
        printf("E");
    }
    else{
        printf("F");
    }

    printf("\n");       //for formatting

    /*I'm not sure if the question requires a special condition of F should any subject have less than 40, but if it says so:
        * change the void main() to int main()
        * in the for loop, add an if condition for marks[i]<40
        * print F and return 0;
    */
}