/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART A: Problem 4

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variable
    float num;

    //User input
    printf("Enter a number: ");
    scanf("%f", &num);

    //Conditional operator +/- test
    printf("Entered number is ");
    ((num>0)?(printf("positive")):(num==0?((printf("zero"))):printf("negative")));
    printf("\n");       //formating
}