/*
    * Made by Az-21
    * Sem 1 Lab Test
    * PART B: Problem 9

*/

//Preprocessor
#include<stdio.h>

//Main
void main(){
    //Variables
    float dim1, dim2, area;
    int shapeId;

    //User input: type of shape
    printf("Enter the number associated with your shape\n1: Triangle\n2: Square\n3: Rectangle\n4: Circle\nYour response: ");
    scanf("%d", &shapeId);

    switch(shapeId){
        case 1: //for triangle
            printf("Enter the base and height length of the triangle: ");
            scanf("%f %f", &dim1, &dim2);
            area = 0.5*dim1*dim2;
            break;

        case 2: //for square
            printf("Enter the side length of the square: ");
            scanf("%f", &dim1);
            area = dim1*dim1;
            break;

        case 3: //for rectange
            printf("Enter the side lengths of the rectangle: ");
            scanf("%f %f", &dim1, &dim2);
            area = dim1*dim2;
            break;

        case 4: //for circle
            printf("Enter the radius of the circle: ");
            scanf("%f", &dim1);
            area = 3.14*dim1*dim1;
            break;

        default: //for invalid input
            printf("Please enter a valid shape ID\n");
    }

    //Output
    printf("Area of selected shape is %.2f square unit", area);
}