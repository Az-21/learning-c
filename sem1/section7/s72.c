/*Section 7-2 (pg 87)
  Linear Search*/

//Preprocessor
#include<stdio.h>

//Main
int main(){
    //Variable
    int i, numFind, myArray[40], arraySize;

    //User input array
    printf("Enter the size of the array: ");
    scanf("%d", &arraySize);


    printf("Populate your array with integers\n");
    for(i=0; i<arraySize; i++){
        scanf("%d", &myArray[i]);
    }

    //User input to obtain number to search
    printf("Enter the number you want to search: ");
    scanf("%d", &numFind);

    //Linear search
    for(i=0; i<arraySize; i++){
        if(numFind == myArray[i]){
            printf("The number is present in the array at index = %d\n", i);
            return 0;
        }
    }
    
    printf("The number is not present in the array\n");
    return 0;
}
