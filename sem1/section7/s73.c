/*Section 7-3 (pg 89)
  Binary Search*/

//Preprocessor
#include<stdio.h>

//Main
int main(){
    //Variable
    int i, myArray[40], arraySize, lower, upper, middle, numFind;

    //User input array
    printf("Enter the size of the array: ");
    scanf("%d", &arraySize);


    printf("Populate your array with integers in ascending order\n");
    for(i=0; i<arraySize; i++){
        scanf("%d", &myArray[i]);
    }

    //User input to obtain number to search
    printf("Enter the number you want to search: ");
    scanf("%d", &numFind);

    //Binary search
    lower = 0;
    upper = arraySize-1;
    while(upper>=lower){
            middle = (lower+upper)/2;
            
            if(myArray[middle] == numFind){
                printf("The number is present in the array at index %d", middle);
                return 0;
            }
            else if(myArray[middle] > numFind){
                upper = middle-1;
            }
            else{
                lower = middle+1;
            }
    }
    
    printf("The number is not present in the array\n");
    return 0;
}
